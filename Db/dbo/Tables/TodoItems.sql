﻿CREATE TABLE [dbo].[TodoItems] (
    [Id]          INT            IDENTITY (1, 1) NOT NULL,
    [Description] NVARCHAR (MAX) NOT NULL,
    [AddedAt]     DATETIME       NOT NULL,
    [AddedById]   NVARCHAR (128) NOT NULL,
    [WasDone]     BIT            NOT NULL,
    [WasDoneAt]   DATETIME       NOT NULL,
    [DueDate]     DATETIME       NULL,
    CONSTRAINT [PK_TodoItems] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_Item_User] FOREIGN KEY ([AddedById]) REFERENCES [dbo].[AspNetUsers] ([Id])
);


GO
CREATE NONCLUSTERED INDEX [IX_FK_Item_User]
    ON [dbo].[TodoItems]([AddedById] ASC);

