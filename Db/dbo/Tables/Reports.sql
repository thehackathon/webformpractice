﻿CREATE TABLE [dbo].[Reports] (
    [Id]                INT            IDENTITY (1, 1) NOT NULL,
    [Time]              DATETIME       NOT NULL,
    [Message]           NVARCHAR (MAX) NOT NULL,
    [StackTrace]        NVARCHAR (MAX) NOT NULL,
    [Type]              NVARCHAR (MAX) NOT NULL,
    [UserId]            NVARCHAR (MAX) NULL,
    [ParentExceptionId] INT            NULL,
    CONSTRAINT [PK_Reports] PRIMARY KEY CLUSTERED ([Id] ASC)
);

