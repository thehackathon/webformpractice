﻿PRINT N'post deployment script..';

DECLARE @userId NVARCHAR(128);
SET @userId = '833bf645-b448-452c-a8d2-7a5605332a65';

INSERT INTO AspNetUsers (Id, EmailConfirmed, PasswordHash, SecurityStamp, PhoneNumberConfirmed, TwoFactorEnabled, LockoutEnabled, AccessFailedCount, UserName)
VALUES
(@userId, 0, 'AJL5SRFhY9FmGcvIgXQGHuuiZh54KXzqW3N1A9I4iHukQOvw6YBrL+MuM+1VjtCMcQ==', 'c0e2aa28-1f59-4d5b-a83b-1899a7ba3028', 0, 0, 0, 0, 'superdev');


INSERT INTO dbo.TodoItems ([Description], [AddedAt], [AddedById], [WasDone], [WasDoneAt], [DueDate])
VALUES 
('test value 1', CURRENT_TIMESTAMP, @userId , 0, CURRENT_TIMESTAMP, '2019-12-31'),
('test value 2', CURRENT_TIMESTAMP, @userId , 1, CURRENT_TIMESTAMP, NULL);
