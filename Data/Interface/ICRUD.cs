﻿namespace Data.Interface
{
    using System.Collections.Generic;

    public interface ICRUD<T> where T : class
    {
        IEnumerable<T> Get(string username);
        T Get(string username, int id);
        int Post(string username, T obj);
        bool Update(string username, int id, T updatedObj);
        bool Delete(string username, int id);
    }
}