﻿namespace Data.Interface
{
    using Tables;
    using System.Collections.Generic;

    public interface ITodoItemsService : ICRUD<TodoItem>
    {
        
    }
}