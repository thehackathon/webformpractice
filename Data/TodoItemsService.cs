﻿namespace Data
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Data.Log;
    using Data.Log.SqlLogger;
    using Interface;
    using Tables;

    public class TodoItemsService : ITodoItemsService
    {
        #region fields

        private const int ReturnIntError = -1;
        private readonly ILog Logger;

        #endregion
        #region constructor

        public TodoItemsService()
        {
            Logger = new SqlLogger();
        }

        #endregion
        #region public methods

        public bool Delete(string username, int id)
        {
            var context = GetContext();
            var item = GetItem(context, username, id);
            if (item != null)
            {
                try
                {
                    context.TodoItems.Remove(item);
                    context.SaveChanges();
                    return true;
                }
                catch (Exception ex)
                {
                    Logger?.Error(ex);
                }
            }
            return false;
        }

        public IEnumerable<TodoItem> Get(string username)
        {
            var context = GetContext();
            var user = GetUserByUsername(context, username);
            var items = user?
                .TodoItems
                .OrderByDescending(item => item.Id)
                .ToList();

            return items ?? new List<TodoItem>();
        }

        public TodoItem Get(string username, int id)
        {
            var context = GetContext();
            var item = GetItem(context, username, id);
            return item;
        }

        public int Post(string username, TodoItem item)
        {
            if (string.IsNullOrWhiteSpace(item.Description))
            {
                return ReturnIntError;
            }
            var context = GetContext();
            var user = GetUserByUsername(context, username);
            var items = user?.TodoItems;
            if (items != null)
            {
                if (item.DueDate == DateTime.MinValue)
                {
                    item.DueDate = null;
                }
                item.AddedAt = DateTime.Now;
                item.AddedBy = user;
                items.Add(item);
                try
                {
                    context.SaveChanges();
                    return item.Id;
                }
                catch (Exception ex)
                {
                    Logger?.Error(ex);
                }
            }
            return ReturnIntError;
        }

        public bool Update(string username, int id, TodoItem updatedItem)
        {
            var context = GetContext();
            var item = GetItem(context, username, id);

            if (item != null)
            {
                UpdateItem(item, updatedItem);
                try
                {
                    context.SaveChanges();
                    return true;
                }
                catch (Exception ex)
                {
                    Logger?.Error(ex);
                }
            }
            return false;
        }

        #endregion
        #region private methods

        private void UpdateItem(TodoItem item, TodoItem updatedItem)
        {
            if (!string.IsNullOrWhiteSpace(updatedItem.Description) && !updatedItem.Description.Equals(item.Description))
            {
                item.Description = updatedItem.Description;
            }

            if (!updatedItem.WasDone.Equals(item.WasDone))
            {
                if (updatedItem.WasDone && !item.WasDone)
                {
                    item.WasDoneAt = DateTime.Now;
                }
                item.WasDone = updatedItem.WasDone;
            }

            if (!updatedItem.DueDate.Equals(item.DueDate))
            {
                item.DueDate = updatedItem.DueDate;
            }
        }

        private TodoItemContainer GetContext()
        {
            var context = new TodoItemContainer();
            return context;
        }

        private TodoItem GetItem(TodoItemContainer context, string username, int id)
        {
            var user = GetUserByUsername(context, username);
            var item = user?.TodoItems
                .SingleOrDefault(it => it.Id.Equals(id));

            return item;
        }

        private AspNetUser GetUserByUsername(TodoItemContainer context, string username)
        {
            var user = context.AspNetUsers
                .SingleOrDefault(u => u.UserName.Equals(username, StringComparison.OrdinalIgnoreCase));

            return user;
        }

        #endregion
    }
}
