﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeFile="Item.aspx.cs" Inherits="WebFormPractice.Item" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="Server">
    <div class="itemForm">
        <div class="row">
            <h2><%=Action.ToString()%></h2>
            <% if (Action.Equals(WebFormPractice.Enums.PageAction.Update)) { %>
            <asp:Button ID="ClearButton" CssClass="btn load-old-ver-btn" runat="server" Text="load saved" OnClick="ClearButton_Click"></asp:Button>
            <% } %>
        </div>
        <asp:UpdatePanel ID="UpdatePanel1" runat="server"
            UpdateMode="Conditional">
            <ContentTemplate>
                <asp:TextBox ID="DescriptionTextArea" TextMode="multiline" Columns="50" Rows="5" runat="server" placeholder="description" />
                <asp:RequiredFieldValidator ID="textAreaError"
                    runat="server" ControlToValidate="descriptionTextArea"
                    ErrorMessage="Please fill description"
                    InitialValue="">
                </asp:RequiredFieldValidator>
                <div class="row">
                    <asp:CheckBox Text="was done" ID="WasDone" runat="server" />
                </div>
                <asp:CheckBox ID="DueDateCheckbox" CssClass="dueDateCheckbox row" Text="due date" runat="server" />
                <div class="row">
                    <asp:Calendar ID="Calendar" runat="server" CssClass="calendar"></asp:Calendar>
                </div>
                <asp:Button Text="submit" runat="server" OnClick="Save_Click" />
            </ContentTemplate>
            <Triggers>
                <asp:AsyncPostBackTrigger ControlID="ClearButton"
                    EventName="Click" />
            </Triggers>
        </asp:UpdatePanel>
    </div>
    <script src="/Scripts/WebForms/CreateItemPageScript.js"></script>
</asp:Content>
