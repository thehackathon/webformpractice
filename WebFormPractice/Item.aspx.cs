﻿namespace WebFormPractice
{
    using Data;
    using Data.Interface;
    using Data.Tables;
    using System;
    using System.Web.UI;
    using WebFormPractice.Enums;

    public partial class Item : Page
    {
        #region enum

        private static class Redirect
        {
            public static readonly string Create = $"/Item?action={PageAction.Create}";
            public const string Created = "/Items";
            public const string Updated = "/Items";
            public const string UnAuthenticated = "/";
        }

        #endregion
        #region private field
        
        private const int AnswerError = -1;

        private ITodoItemsService service;

        #endregion
        #region prop

        public int Id { get; set; }
        public PageAction Action { get; set; }

        #endregion
        #region constructor
        public Item()
        {
            service = new TodoItemsService();
        }
        #endregion
        #region event handler

        protected void Page_PreInit(object sender, EventArgs e)
        {
            if (!User.Identity.IsAuthenticated)
            {
                Response.Redirect(Redirect.UnAuthenticated);
            }
            else
            {
                var actionString = Request.Params["action"];
                if (!string.IsNullOrWhiteSpace(actionString) && int.TryParse(actionString, out int result))
                {
                    Action = (PageAction)Enum.ToObject(typeof(PageAction), result);
                }
                else
                {
                    Action = PageAction.Create;
                }
            }
        }

        protected void Page_Init(object sender, EventArgs e)
        {
            switch (Action)
            {
                case PageAction.Update:
                {
                    var idString = Request.Params["id"];
                    if (!string.IsNullOrWhiteSpace(idString) && int.TryParse(idString, out int id))
                    {
                        var username = User.Identity.Name;
                        var item = service.Get(username, id);
                        if (item != null)
                        {
                            Id = id;
                            SetitemToPage(item);
                        }
                        else
                        {
                            Response.Redirect(Redirect.Create);
                        }
                    }
                }
                break;
            }
        }

        protected void Save_Click(object sender, EventArgs e)
        {
            var username = User.Identity.Name;
            var item = GetItemFromPage();

            if (string.IsNullOrWhiteSpace(DescriptionTextArea.Text))
            {
                Response.StatusCode = (int)ResponseCode.BadRequest;
            }

            switch (Action)
            {
                case PageAction.Create:
                {
                    Create(username, item);
                }
                break;
                case PageAction.Update:
                {
                    Update(username, item);
                }
                break;
            }
        }
        protected void ClearButton_Click(object sender, EventArgs e)
        {
            var username = User.Identity.Name;
            var item = service.Get(username, Id);
            if (item != null)
            {
                SetitemToPage(item);
            }
        }

        #endregion
        #region private methods

        private void Update(string username, TodoItem item)
        {
            var success = service.Update(username, Id, item);
            if (success)
            {
                Response.Redirect(Redirect.Updated);
            }
            else
            {
                Response.StatusCode = (int)ResponseCode.BadRequest;
            }
        }

        private void Create(string username, TodoItem item)
        {
            var answer = service.Post(username, item);
            if (answer == AnswerError)
            {
                Response.StatusCode = (int)ResponseCode.BadRequest;
            }
            else
            {
                Response.Redirect(Redirect.Created);
            }
        }

        private TodoItem GetItemFromPage()
        {
            var description = DescriptionTextArea.Text;
            var wasDone = WasDone.Checked;
            var wasDoneAt = DateTime.Now;
            DateTime? dueDate;
            if (!DueDateCheckbox.Checked || Calendar.SelectedDate == DateTime.MinValue)
            {
                dueDate = null;
            }
            else
            {
                dueDate = Calendar.SelectedDate;
            }

            var item = new TodoItem
            {
                Description = description,
                DueDate = dueDate,
                WasDone = wasDone,
                WasDoneAt = wasDoneAt,
            };

            return item;
        }

        private void SetitemToPage(TodoItem item)
        {
            DescriptionTextArea.Text = item.Description;
            WasDone.Checked = item.WasDone;
            if (item.DueDate != null)
            {
                Calendar.SelectedDate = (DateTime)item.DueDate;
                DueDateCheckbox.Checked = true;
            }
            else
            {
                Calendar.SelectedDate = DateTime.MinValue;
                DueDateCheckbox.Checked = false;
            }
        }

        #endregion
    }
}