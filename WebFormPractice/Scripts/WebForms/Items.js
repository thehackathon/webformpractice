﻿const state = {
    create: 2,
    update: 4,
};

function updateItem(grid, args) {
    const dataItem = args.get_gridDataItem();
    const id = dataItem._element.cells[0].textContent;
    location.href = `/Item?action=${state.update}&id=${id}`;
}