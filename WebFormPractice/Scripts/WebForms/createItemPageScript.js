﻿$(document).ready(() => {
    checkDueDate();

    const root = document.querySelector(".itemForm");
    const mutationObserver = new MutationObserver(function (mutations) {
        checkDueDate();
    });

    mutationObserver.observe(root, {
        childList: true,
        subtree: true,
    });
});

function checkDueDate() {
    const elem = $(".dueDateCheckbox input[type=checkbox]:checked");
    if (elem.length === 0) {
        $(".calendar").addClass("hide");
    }

    $(".dueDateCheckbox").on("change", () => {
        $(".calendar").toggleClass("hide");
    });
}