﻿namespace WebFormPractice
{
    using System;
    using System.Web.UI;

    public partial class _Default : Page
    {
        #region redirect address

        private const string AuthenticatedRedirect = "/Items";

        #endregion
        #region event handler

        protected void Page_PreInit(object sender, EventArgs e)
        {
            if (User.Identity.IsAuthenticated)
            {
                Response.Redirect(AuthenticatedRedirect);
            }
        }

        #endregion
    }
}