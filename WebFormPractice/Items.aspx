﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeFile="Items.aspx.cs" Inherits="WebFormPractice.Items" %>

<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="Server">
    <link href="/Content/items.css" rel="stylesheet" />
    <div class="buttons">
        <a href="/Item" rel="stylesheet">Create</a>
    </div>
    <div class="tableContainer">
        <asp:ObjectDataSource runat="server" ID="odsTodoItems" SelectMethod="GetItems" EnablePaging="true" SortParameterName="sortParameter" />

        <telerik:RadAjaxPanel ID="RadAjaxPanel1" runat="server" LoadingPanelID="RadAjaxLoadingPanel1">
            <telerik:RadGrid RenderMode="Lightweight" ID="RadGrid1" DataSourceID="odsTodoItems" runat="server" PageSize="5"
                AllowSorting="True" AllowMultiRowSelection="True" AllowPaging="True" ShowGroupPanel="True"
                AutoGenerateColumns="False" GridLines="none" CssClass="table" AllowAutomaticDeletes="True" AllowAutomaticInserts="True" AllowAutomaticUpdates="True"
                OnDeleteCommand="RadGrid1_DeleteCommand">
                <PagerStyle Mode="NextPrevAndNumeric" Position="Top" Visible="true"></PagerStyle>
                <MasterTableView Width="100%" ItemStyle-CssClass="click-row">
                    <Columns>
                        <telerik:GridBoundColumn SortExpression="Id" HeaderText="Id" HeaderButtonType="TextButton"
                            DataField="Id">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn SortExpression="Description" HeaderText="Description" HeaderButtonType="TextButton"
                            DataField="Description">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn SortExpression="WasDone" HeaderText="Was Done" HeaderButtonType="TextButton"
                            DataField="WasDone">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn SortExpression="AddedAt" HeaderText="Added at" HeaderButtonType="TextButton"
                            DataField="AddedAt">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn SortExpression="AddedBy" HeaderText="Added by" HeaderButtonType="TextButton"
                            DataField="AddedBy.UserName">
                        </telerik:GridBoundColumn>
                        <telerik:GridButtonColumn HeaderText="Actions" Text="Delete" ButtonType="LinkButton" CommandName="Delete">
                        </telerik:GridButtonColumn>
                    </Columns>
                </MasterTableView>
                <ClientSettings AllowDragToGroup="False" AllowColumnsReorder="False" EnableRowHoverStyle="true" EnablePostBackOnRowClick="true">
                    <Selecting AllowRowSelect="False"></Selecting>
                    <Resizing AllowRowResize="False" AllowColumnResize="True" EnableRealTimeResize="True"
                        ResizeGridOnColumnResize="False"></Resizing>
                    <ClientEvents OnRowClick="updateItem" />
                </ClientSettings>
                <GroupingSettings ShowUnGroupButton="false"></GroupingSettings>
            </telerik:RadGrid>
        </telerik:RadAjaxPanel>
    </div>

    <script src="/Scripts/WebForms/Items.js"></script>
</asp:Content>
