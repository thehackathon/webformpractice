﻿namespace WebFormPractice
{
    using Data;
    using Data.Interface;
    using Data.Tables;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web.UI.WebControls;
    using Telerik.Web.UI;
    using WebFormPractice.Enums;

    public partial class Items : System.Web.UI.Page
    {
        #region enum

        private static class Commands
        {
            public const string Delete = "Delete";
        }

        private static class Redirect
        {
            public const string Delete = "/Items";
            public const string UnAuthenticated = "/";
        }

        private static class SortHelper
        {
            public const string AscMarker = " ASC";
            public const string DescMarker = " DESC";
        }

        #endregion
        #region private field

        private ITodoItemsService service;

        #endregion
        #region constructor

        public Items()
        {
            service = new TodoItemsService();
        }

        #endregion
        #region event handler

        protected void Page_Init(object sender, EventArgs e)
        {
            if (!User.Identity.IsAuthenticated)
            {
                Response.Redirect(Redirect.UnAuthenticated);
            }
            else
            {
                odsTodoItems.TypeName = this.GetType().AssemblyQualifiedName;
            }
        }


        public IEnumerable<TodoItem> GetItems(int startRowIndex, int maximumRows, string sortParameter)
        {
            var username = User.Identity.Name;
            var items = service.Get(username);

            if (!string.IsNullOrWhiteSpace(sortParameter))
            {
                var isDesc = sortParameter.Contains(SortHelper.DescMarker);
                sortParameter = sortParameter.Replace(
                    isDesc 
                        ? SortHelper.DescMarker 
                        : SortHelper.AscMarker, 
                    string.Empty);
                items = isDesc
                    ? items.AsQueryable().OrderByDescending(it => GetPropertyValue(it, sortParameter))
                    : items.AsQueryable().OrderBy(it => GetPropertyValue(it, sortParameter));
            }

            return items.ToList();
        }

        protected void RadGrid1_DeleteCommand(object sender, GridCommandEventArgs e)
        {
            var cellIdIdx = 2;
            var dataItem = (GridDataItem)e.Item;
            var type = dataItem.Cells[cellIdIdx];

            // TODO: как-то получить обьект по которому кликнули (TodoItem) вместо того что бы доставать из колонки
            if (dataItem.Cells[2] is GridTableCell cellId)
            {
                if (int.TryParse(cellId.Text, out var id))
                {
                    Delete(id);
                }
            }
        }

        #endregion
        #region private methods

        private void Delete(int id)
        {
            var username = User.Identity.Name;
            var success = service.Delete(username, id);
            if (success)
            {
                Response.Redirect(Redirect.Delete);
            }
            else
            {
                Response.StatusCode = (int)ResponseCode.BadRequest;
            }
        }

        private static object GetPropertyValue(object obj, string property)
        {
            var propertyInfo = obj.GetType().GetProperty(property);
            return propertyInfo.GetValue(obj, null);
        }

        #endregion
    }
}